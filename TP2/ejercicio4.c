#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define Hijos 20
/*variables globales*/
int cont_hijos=0;
pid_t PID_hijos[Hijos];

void handler_SIGUSR1(int);
void handler_SIGUSR2(int);
void handler_ALRM(int);

int main()
{
/* manejador de señales para SIGUSR1*/
signal(SIGUSR1, handler_SIGUSR1);
/* manejador de señales para SIGUSR1*/
signal(SIGUSR2, handler_SIGUSR2);
/* manejador de señales para SIGALRM*/
signal(SIGALRM, handler_ALRM);

printf("Identificativo de proceso: %d\n", getpid());
printf("Envie la señal SIGUSR1 para crear un nuevo hijo\n");
printf("Envie la señal SIGUSR2 para terminar con la ejecucion de todos los hijos\n");

while(1){
    /*Configuro alarma para que se active cada 10 segundos*/
    alarm(10);
    pause();
}

return 0;
}

void handler_SIGUSR1(int sig1)
{
    pid_t var;
    var=fork();

    if (var==0)
        {
        while(1){
            printf("\tsoy el hijo y mi PID es %d \n",getpid());
            printf("\tEl PID de mi Padre es %d \n\n",getppid());
            sleep(5);//espera 5 segundos hasta imprimir nuevamente
            }
        }
    else{
        PID_hijos[cont_hijos]=var;
        cont_hijos++;
    }
}

/*tengo un problema con la impresion en pantalla: como uno imprime cada 
5 y otro cada 10 se me superponen. Podriamos poner que el padre espere 
a que los hijos terminen pero ya no estaria imprimiendo cada 10 seg*/
void handler_ALRM(int sig2)
{
    int i;
    printf("soy el padre y tengo %d hijos\n",cont_hijos);
    printf("los PIDs de mis hijos son:\n" );

    for(i=0;i<cont_hijos;i++){
        printf("PID del proceso hijo[%d]: %d",i,PID_hijos[i]);
    }
}

void handler_SIGUSR2(int sig3)
{
    int i;
    for(i=0;i<cont_hijos;i++){
        kill(SIGKILL,PID_hijos[i]);
    }
    printf("se terminó la ejecucion de todos los hijos creados\n");
}