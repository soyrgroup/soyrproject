/*
## Ejecución del programa

En primera instancia se debe compilar el código:

"gcc -o <nombre del ejecutable> ./ejercicio6.c"

Luego de haber compilado, se ejecuta el programa.

"./<nombre del ejecutable>"



## Envio de señales

 - Para enviar la señal SIGUSR1 se puede, desde una segunda consola, utilizar el siguiente comando:

"kill -s SIGUSR1 <PID del proceso padre>"

 - Para enviar la señal SIGUSR2 se puede, desde una segunda consola, utilizar el siguiente comando:

"kill -s SIGUSR2 <PID del proceso padre>"

 - Para enviar la señal SIGTERM se puede, desde una segunda consola, utilizar el siguiente comando:

"kill -s SIGTERM <PID del proceso padre>"

*/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>


void handler_SIGUSR1(int);
void handler_SIGUSR2(int);
void handler_SIGTERM(int);
void pileUp(pid_t);
void nameThem(void);
void nameMyself(void);
void exec_ps(void);
void killYoungest(void);

struct bunkBed{
    pid_t PID_hijo;
    struct bunkBed *below;
};
typedef struct bunkBed bunkBed_t;

/*variables globales*/
char flag1=0,flag2=0,flagT=0;
bunkBed_t *topBed = NULL;
pid_t var1;
pid_t var2;

int main(){
    /* manejador de señales para SIGUSR1*/
    signal(SIGUSR1, handler_SIGUSR1);
    /* manejador de señales para SIGUSR1*/
    signal(SIGUSR2, handler_SIGUSR2);
    /* manejador de señales para SIGTERM*/
    signal(SIGTERM, handler_SIGTERM);

    printf("Identificativo de proceso: %d\n", getpid());
    printf("Envie la señal SIGUSR1 para crear un nuevo hijo\n");
    printf("Envie la señal SIGUSR2 para crear un hijo que ejecute el programa: ps -l \n");
    printf("Envie la señal SIGTERM para terminar con los hijos creados con SIGUSR1 \n\n");

    while(1){ 
        pause();
        if(flag1){
            if ((var1=fork())== 0){
                while(1){
                    nameMyself();
                    sleep(5);//espera 5 segundos hasta imprimir nuevamente
                }
            }else{
                flag1=0;
                pileUp(var1);
                nameThem();
                
            }
        }
        if(flag2){
            if ((var2= fork()) ==0){
                exec_ps();
                exit(0);
            }
            flag2=0;
        }
        if(flagT){
            while(topBed != NULL){
                killYoungest();
                sleep(1);
            }
            /*finalmente se termina el padre*/
            printf("\e[0;31m Se termina el proceso padre %d \e[0;37m \n",getpid());
            exit(0);
        }
    }
    return 0;
}

//Signal Handlers
void handler_SIGUSR1(int sig1){
    flag1=1;
}

void handler_SIGUSR2(int sig2){
    flag2=1;
}

void handler_SIGTERM(int sig3){
    flagT=1;
}


//Pile managers
void pileUp(pid_t newBorn){
    bunkBed_t *newBed = (bunkBed_t *) malloc( sizeof(bunkBed_t) );
    newBed->PID_hijo = newBorn;
    newBed->below = topBed;
    topBed = newBed;
    return;
}

void nameMyself(void){
    printf("\e[0;97m\n\tSoy el hijo y mi PID es %d \n",getpid());
    printf("\tEl PID de mi Padre es %d \n\n",getppid());
}

void nameThem(void){
    bunkBed_t *spotlight = topBed;
    int i = 1;
    while(spotlight != NULL){
        printf("PID hijo %d: %d\n",i,spotlight->PID_hijo);
        i++;
        spotlight = spotlight->below;
    }
    printf("\n");
    return;
}

void exec_ps(void){
    char programa[]="/bin/ps";
    char *args[]={"/bin/ps","-l",NULL};
    printf("\e[0;93m\n\tsoy el hijo y mi PID es %d \n",getpid());
    printf( "\tahora voy a ejecutar el comando '%s %s %s'\n", programa,args[0], args[1]);
    execv(programa,args);
}

void killYoungest(void){
    bunkBed_t *aim = topBed;
    printf("\e[0;31m Se termina el proceso hijo %d \e[0;37m \n\n", aim->PID_hijo);
    kill(aim->PID_hijo,SIGKILL);
    topBed = aim->below;
    free(aim);
    return;
}