# Ejercicio 6: Programas padres, hijos

El archivo ejercicio6.c contiene un programa que al ejecutar recibe señales y las procesa.

 - Al recibir la señal SIGUSR1 crea un proceso hijo que imprime su PID y el del padre cada 5 segundos. 

 - Al recibir la señal SIGUSR2 crea un proceso hijo que imprime su PID, ejecuta el programa "ps -l" y termina.

 - Al recibir la señal SIGTERM, el proceso padre termina todos los procesos creados por la recepción de la señal SIGUSR1, a razón de 1 por segundo. Al terminar cada proceso se muestra el PID de este.

## Ejecución del programa

En primera instancia se debe compilar el código:

```bash
gcc -o <nombre del ejecutable> ./ejercicio6.c
```

Luego de haber compilado, se ejecuta el programa.

```bash
./<nombre del ejecutable>
```
## Envio de señales

 - Para enviar la señal SIGUSR1 se puede, desde una segunda consola, utilizar el siguiente comando:

```bash
kill -s SIGUSR1 <PID del proceso padre>
```

 - Para enviar la señal SIGUSR2 se puede, desde una segunda consola, utilizar el siguiente comando:

```bash
kill -s SIGUSR2 <PID del proceso padre>
```

 - Para enviar la señal SIGTERM se puede, desde una segunda consola, utilizar el siguiente comando:

```bash
kill -s SIGTERM <PID del proceso padre>
```

## Normal output

Cuando se inicia el programa se imprime en pantalla un mensaje como el siguiente, por única vez:

```bash
Identificativo de proceso: 2440
Envie la señal SIGUSR1 para crear un nuevo hijo
Envie la señal SIGUSR2 para crear un hijo que ejecute el programa: ps -l 
Envie la señal SIGTERM para terminar con los hijos creadis con  SIGUSR1
```

Se puede observar en este ejemplo como se muestra en pantalla el identificador de programa.


Si se le envía al proceso la señal SIGUSR1 se mostrará en pantalla por única vez una lista con todos los hijos creados en base a la recepción de dicha señal, en conjunto con un mensaje como el siguiente:

```bash
Soy el hijo y mi PID es 2507 
El PID de mi Padre es 2440 
```

Dicho mensaje será repetido por el proceso hijo cada cinco segundos.

Si se le envía al proceso la señal SIGUSR2 se mostrará en pantalla por única vez el resultado de la ejecución del comando de bash

```bash
ps -l
```

Si se le envía al proceso la señal SIGTERM, se terminarán los procesos hijos creados, uno a uno y cada vez que esto suceda se mostrará en pantalla un mensaje como el siguiente:

```bash
Se termina el proceso hijo 2507
```
Por último, luego de terminar los procesos hijo, el proceso padre llega a su fin y termina con un mensaje como el siguiente:

```bash
Se termina el proceso padre 2440
```
