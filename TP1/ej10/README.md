# Script ej10

El script ej10 contiene un conjunto de comandos de bash que permite pasarle como argumento un directorio, y responde con la cantidad de directorios, archivos regulares y otros, contenidos en el mismo.

## Formato para el uso

```bash
./ej10.sh < directorio >
```

## Salida normal

Si el script es utilizado correctamente se deberia imprimir en pantalla la salida en el siguiente formato:

```bash
Cantidad de directorios en < directorio >: < número de directorios >
Cantidad de archivos regulares en < directorio >: < número de archivos regulares >
Cantidad de otros archivos en < directorio >: < número de otros archivos >
```

## Errores

Si se le pasa más de un argumento o ninguno se imprimirá en pantalla el siguiente mensaje:

```bash
Cantidad de argumentos incorrecto
```

Si se le pasa como argumento algo distinto a un directorio o un directorio sin permiso de lectura para el usuario actual, se imprimirá en pantalla el siguiente mensaje:

```bash
<directorio> no es un directorio o no es accesible por el usuario actual.
```