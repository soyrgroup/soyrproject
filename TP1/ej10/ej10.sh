#! /bin/bash

#made by: somosR


if [ $# -ne 1 ]; then           #si cant de argumentos es distinta a 1...
    echo "Cantidad de argumentos incorrecto"
    exit
fi

dir="$1"
if [ ! -d "$dir" ]; then        #si argumento no es directorio accesible
    echo "$dir no es un directorio o no es accesible por el usuario actual."
    exit
fi

#Declaración e inicialización de contadores
ndirs=0
regFiles=0
otherFiles=0


for thing in "$dir"/*; do       #Recorre elementos en el directorio

    #Clasificación
    if [ -d "$thing" ]; then    # Es un directorio
        ((ndirs++))

    elif [ -f "$thing" ]; then  # Es un archivo regular
        ((regFiles++))

    else                        # No es un dir ni un archivo regular
        ((otherFiles++))

    fi
done

echo "Cantidad de directorios en "$dir": $ndirs"
echo "Cantidad de archivos regulares en "$dir": $regFiles"
echo "Cantidad de otros archivos en "$dir": $otherFiles"
