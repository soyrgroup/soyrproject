#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct {
    int vector[80];
    int largo;
} numeros;

void handler_SIGUSR1(int);
void handler_SIGUSR2(int);
void handler_SIGTERM(int);
int sumaN(int *, int);
void *sumaNth(void *);

char flag1 = 0, flag2 = 0, flagT = 1;

int main(int argc, char *argv[]) {
    int N, vector[80], rc, res = 0,*r;
    pthread_t tid;
    numeros num;

    if (argc != 2) {
        printf("Uso: %s nro_de_threads\n", argv[0]);
        exit(1);
    }
    sscanf(argv[1], "%d", &N);
    num.largo = N;

    // Manejador de señales
    signal(SIGUSR1, handler_SIGUSR1);
    signal(SIGUSR2, handler_SIGUSR2);
    signal(SIGTERM, handler_SIGTERM);

    for (int j = 0; j < N; j++) {
        vector[j] = j + 1;
        printf("vector[%d] = %d\n", j, vector[j]);
        num.vector[j] = j + 1;
    }
    printf("Identificativo de proceso: %d\n", getpid());
    printf("Envía la señal SIGUSR1 para crear un hijo que haga la suma\n");
    printf("Esperando a recibir alguna señal\n");
    pause();

    while (flagT) {
        if (flag1) {
            if (fork() == 0) {
                printf("Soy el hijo y voy a ejecutar la suma\n");
                res = sumaN(vector, N);
                printf("La suma de los primeros %d números es: %d\n\n", N, res);
                exit(0);
            } else {
                printf("Soy el padre. Procedo a desactivar la bandera\n");
                flag1 = 0;
            }
        }
        if (flag2) {
            if (pthread_create(&tid, NULL, sumaNth, (void *)&num)) {
                printf("ERROR: No se pudo crear el hilo\n");
                exit(-1);
            }
            printf("Soy el padre. Procedo a desactivar la bandera\n");
            flag2 = 0;
            rc = pthread_join(tid, (void *)&r);
            if (rc) {
                printf("ERROR: El hilo ha terminado con error\n");
                exit(-1);
            }
            res=*r;
            printf("La suma de los primeros %d números es: %d\n\n", N,res);
        }
    }
    exit(0);
}

void handler_SIGUSR1(int sig1) {
    flag1 = 1;
}

void handler_SIGUSR2(int sig2) {
    flag2 = 1;
}

void handler_SIGTERM(int sig3) {
    flagT = 0;
}

int sumaN(int *v, int N) {
    int r = 0;
    for (int j = 0; j < N; j++) {
        r += v[j];
    }
    return r;
}

void *sumaNth(void *arg) {
    int *r;
    *r=0;
    numeros *v = (numeros *)arg;
    for (int j = 0; j < v->largo; j++) {
        *r = *r + v->vector[j];
    }
    pthread_exit((void *)r);
}
