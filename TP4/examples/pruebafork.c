#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


void do_nothing() {
int i;
i= 0;
}

int main(int argc, char *argv[]) {
int pid, j, status,NFORKS;
if (argc!=2)
{
printf("uso: pf nro_de_forks\n");
exit(1);
}
sscanf(argv[1],"%d",&NFORKS);
for (j=0; j<NFORKS; j++) {

  /*** Manejo de error ***/
  if ((pid = fork()) < 0 ) {
    printf ("fork fallo con codigo de error= %d\n", pid);
    exit(0);
    }

  /*** Este es el hijo ***/
  else if (pid ==0) {
    do_nothing();
    exit(0);
    }

  /*** este es el padre ***/
  else {
    waitpid(pid, &status, 0);
    }
  }
}  

