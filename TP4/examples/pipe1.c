 #include <sys/types.h>
       #include <sys/wait.h>
       #include <stdio.h>
       #include <stdlib.h>
       #include <unistd.h>
       #include <string.h>

       int
       main(int argc, char *argv[])
       {
           int pipefd[2];
           pid_t cpid;
           char buf;
           char buffer[81];

          
           if (pipe(pipefd) == -1) {
               perror("pipe");
               exit(EXIT_FAILURE);
           }

           cpid = fork();
           if (cpid == 0) {    /* Child reads from pipe */
               close(pipefd[1]);          /* Close unused write end */
               write(STDOUT_FILENO,"SOY EL HIJO \n",14);

               while (read(pipefd[0], buffer, 80) > 0);
                   

               write(STDOUT_FILENO, "RECIBI EL MENSAJE DE MI PADRE\n", 31);
               write(STDOUT_FILENO, buffer, strlen(buffer));
               close(pipefd[0]);
               _exit(EXIT_SUCCESS);

           } else {            /* Parent writes to pipe */
               close(pipefd[0]);          /* Close unused read end */
               fprintf(stdout,"Soy el padre, ingrese un mensaje para enviar al hijo\n");
               fgets(buffer,80,stdin);
               write(pipefd[1], buffer, strlen(buffer));
               close(pipefd[1]);          /* Reader will see EOF */
               wait(NULL);                /* Wait for child */
               exit(EXIT_SUCCESS);
           }
       }
