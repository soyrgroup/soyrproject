# Ejercicio 11: BUFFER PING PONG

Los archivos cons.c y prod.c contienen programas que funcionando en conjunto implementan un BUFFER PING PONG.

 - El productor lee datos desde el archivo de entrada ("quijoteIN.txt") y los escribe en el Buffer, en donde cada datos es una linea de texto. El productor escribe en el Buffer las lineas del archivo que contienen texto, a razón de un linea por segundo. Además, para diferenciar el texto producido por cada productor, la linea empieza con P1: o P2: segun corresponda.

 - El consumidor lee desde el Buffer, imprime lo leido y lo almacena en el archivo de salida ("quijoteOUT.txt")

## Ejecución del programa

En primera instancia se deben compilar los programas:

```bash
gcc -o <nombre del ejecutable del programa cons> ./cons.c -lpthread -lrt
```

```bash
gcc -o <nombre del ejecutable del programa prod> ./prod.c -lpthread -lrt
```

Luego de haber compilado, se ejecuta el consumidor y el productor. Para que el programa funcione acordemente se debe ejecutar primero al consumidor. Además para una ejecucion normal se tienen 2 procesos productores. 

```bash
./<nombre del ejecutable programa cons> 
```

```bash
./<nombre del ejecutable programa prod> 
```

```bash
./<nombre del ejecutable programa prod> 
```

## Intercambio de productor

Solo se realiza un intercambio si se tienen 2 procesos productores ejecutandose simultaneamente y si se le manda la señal SIGUSR1 al consumidor:

```bash
kill -s SIGUSR1 <PID del proceso consumidor>
```

## Terminación normal del programa

 Para terminar los programas se recomienda enviar la señal SIGTERM  al proceso consumidor desde una cuarta consola, utilizando el siguiente comando:

```bash
kill -s SIGTERM <PID del proceso consumidor>
```