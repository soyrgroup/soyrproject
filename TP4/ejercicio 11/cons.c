#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h> 
#include <sys/mman.h>
#include <fcntl.h>

typedef struct{
    char line[200];
}line_t;

void handler_SIGTERM(int);
void handler_SIGUSR1(int);
void* tryFetch1(line_t*);
void* tryFetch2(line_t*);
void* tryCounting(unsigned int*);
void* tryDisp(line_t*);
void* tryPoet(line_t*);
unsigned int manageTurn(unsigned int);
void consTermination();
void checksum(int);

int flagT = 1;
int chTurn = 0;
int timeOut = 0;
int cserror = 0;
int shmPPB_id;
int shmLCK_id;
int shmPID_id;
line_t *shmPPB_ptr = NULL;
pthread_mutex_t *shmLCK_ptr = NULL;
pid_t *shmPID_ptr = NULL;
FILE* pf;
int FD;


int main(){
    key_t key;
    pthread_t lightFetch1_id;       //thread for fetching from buff. 1
    pthread_t lightFetch2_id;       //thread for fetching from buff. 2
    pthread_t lightTime_id;         //thread for stopping after time limit
    pthread_t lightDisp_id;         //thread for displaying
    pthread_t lightPoet_id;         //thread for writting on file


    signal(SIGTERM, handler_SIGTERM);
    signal(SIGUSR1, handler_SIGUSR1);
    int error;
    
    
    
    struct shmid_ds mem_data;
    line_t localBuff[5];
    unsigned int atWork = 0;
    unsigned int nowBuff = 1;
    


    ///////////////////FILE OPENER////////////////
    /*apretura del file de salida*/
    printf("\nPID consumidor: %d\n",getpid());
    if((pf=fopen("quijoteOUT.txt", "w")) == NULL){
        printf("Error al abrir el archivo\n");
        exit(1);
    }


    ///////////////////PING PONG BUFFER//////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    key = ftok("/bin/ls",137);
    if (key == -1){
        printf("No consegui clave para mc del BUFFER.\n");
        exit(1);
    }
    /*creacion del espacio de memoria compartida*/
    shmPPB_id = shmget(key, 12*sizeof(line_t), 0666 | IPC_CREAT);
    if (shmPPB_id == -1){
        printf("No consegui ID para mc del BUFFER\n");
        exit (2);
    }
    /*Asocicion de un puntero al espacio de memoria*/
    shmPPB_ptr = (line_t *)shmat(shmPPB_id, (const void *)0, 0);
    if (shmPPB_ptr == NULL){
        printf("No consegui asociar la mc del BUFFER a una variable\n");
        exit (3);
    }




    ///////////////////LOCKS/////////////////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    if((FD = shm_open("mutex",O_RDWR|O_CREAT, 0660)) == -1){
        printf("no open");
        exit(1);
    }
    if (ftruncate(FD, 2*sizeof(pthread_mutex_t)) != 0){
        printf("no trunk");
        exit(2);
    }

    shmLCK_ptr = (pthread_mutex_t *)mmap(NULL, 2*sizeof(pthread_mutex_t), PROT_READ|PROT_WRITE, MAP_SHARED, FD, 0);
    if (shmLCK_ptr == MAP_FAILED){
        printf("no map");
        exit(3);
    }

    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
    pthread_mutex_init(&shmLCK_ptr[0], &attr);
    pthread_mutex_init(&shmLCK_ptr[1], &attr);


    ///////////////////PIDs//////////////////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    key = ftok("/bin/clear",65);
    if (key == -1){
        printf("No consegui clave para mc de los PIDs\n");
        exit(9);
    }
    /*creacion del espacio de memoria compartida*/
    shmPID_id = shmget(key, 3*sizeof(pid_t), 0666 | IPC_CREAT);
    if (shmPID_id == -1){
        printf("No consegui ID para mc de los PIDs\n");
        exit (10);
    }
    /*Asocicion de un puntero al espacio de memoria*/
    shmPID_ptr = (pid_t *)shmat(shmPID_id, (const void *)0, 0);
    if (shmPID_ptr == NULL){
        printf("No consegui asociar la mc de los PIDs a una variable\n");
        exit (11);
    } else {
        shmPID_ptr[0] = 0;
        shmPID_ptr[1] = 0;
        shmPID_ptr[2] = getpid();
    }



    shmctl(shmPID_id, IPC_STAT, &mem_data);
    if(mem_data.shm_nattch == 1){
        printf("Esperando que se conecten las torres...\n");
        while(mem_data.shm_nattch<2){
            shmctl(shmPID_id, IPC_STAT, &mem_data);                 
            
            if(flagT == 0){
                consTermination();
                exit(0);
            }
        }
        printf("Empecemos la jornada.\n");
    } else if(mem_data.shm_nattch > 1){
        printf("Soy un proceso intruso. Procedo a terminar.\n");
        shmdt((const void *) shmPPB_ptr);
        shmdt((const void *) shmLCK_ptr);
        shmdt((const void *) shmPID_ptr);

        exit(12);
    }


    

    
    //wait for el disparo
    while( shmPID_ptr[0] == 0 );

    printf("PID productor: %d\n",shmPID_ptr[0]);



    while(flagT){

        
        atWork = manageTurn(atWork);
        nowBuff = 1;

        printf("\n\t\e[1;32mSoy el hilo Fetch del proceso %d, esperando para leer el buffer 1...\e[0m\n\n",getpid());fflush(stdout);
        
        error = pthread_create(&lightFetch1_id, NULL, (void*)tryFetch1, (void *)localBuff);
        if (error != 0) {
            printf("\nThread can't be created :[%s]", strerror(error));
            flagT = 0;
        }
        error = pthread_create(&lightTime_id, NULL, (void*)tryCounting, (void *)&nowBuff);
        if (error != 0) {
            printf("\nThread can't be created :[%s]", strerror(error));
            flagT = 0;
        }
    
        pthread_join(lightFetch1_id, NULL);
        pthread_cancel(lightTime_id);
        printf("\n");
        pthread_join(lightTime_id,NULL);
        
        if(cserror == 0){
            error = pthread_create(&lightDisp_id, NULL, (void*)tryDisp, (void *)localBuff);
            if (error != 0) {
                printf("\nThread can't be created :[%s]", strerror(error));
                flagT = 0;
            }

            error = pthread_create(&lightPoet_id, NULL, (void*)tryPoet, (void *)localBuff);
            if (error != 0) {
                printf("\nThread can't be created :[%s]", strerror(error));
                flagT = 0;
            }

            pthread_join(lightDisp_id, NULL);
            pthread_join(lightPoet_id, NULL);
        }else{
            chTurn = 1;
            cserror = 0;
        }




        atWork = manageTurn(atWork);
        nowBuff = 2;

        if(flagT == 0){
            consTermination();
            exit(0);
        }


        printf("\n\t\e[1;32mSoy el hilo Fetch del proceso %d, esperando para leer el buffer 2...\e[0m\n\n",getpid());
        
        error = pthread_create(&lightFetch2_id, NULL, (void*)tryFetch2, (void *)localBuff);
        if (error != 0) {
            printf("\nThread can't be created :[%s]", strerror(error));
            flagT = 0;
        }
        error = pthread_create(&lightTime_id, NULL, (void*)tryCounting, (void *)&nowBuff);
        if (error != 0) {
            printf("\nThread can't be created :[%s]", strerror(error));
            flagT = 0;
        }

        pthread_join(lightFetch2_id, NULL);
        pthread_cancel(lightTime_id);
        printf("\n");
        pthread_join(lightTime_id,NULL);

        if(cserror == 0){
            error = pthread_create(&lightDisp_id, NULL, (void*)tryDisp, (void *)localBuff);
            if (error != 0) {
                printf("\nThread can't be created :[%s]", strerror(error));
                flagT = 0;
            }

            error = pthread_create(&lightPoet_id, NULL, (void*)tryPoet, (void *)localBuff);
            if (error != 0) {
                printf("\nThread can't be created :[%s]", strerror(error));
                flagT = 0;
            }

            pthread_join(lightDisp_id, NULL); //remember to exit in thread
            pthread_join(lightPoet_id, NULL); //remember to exit in thread
        }else{
            chTurn = 1;
            cserror = 0;
        }

        if(flagT == 0){
            consTermination();
            exit(0);
        }
    
    }

    consTermination();
    exit(0);

}










/////FUNCTIONS/////////////////////

void handler_SIGUSR1(int sig){
    chTurn = 1;
}

void handler_SIGTERM(int sig){
    flagT = 0;
}




void* tryFetch1(line_t* intBuff){

    /*bloqueo buffer 1 para lectura*/
    pthread_mutex_lock(&shmLCK_ptr[0]);


    for(int i=0;i<5;i++){
        strcpy(intBuff[i].line,shmPPB_ptr[i].line);
    }


    checksum(1);
    if(cserror == 1){
        printf("\n\t\e[1;31mHa habido un error en la comunicacion (buffer 1).\e[0m\n");
    }

    /*desbloqueo buffer 1*/
    pthread_mutex_unlock(&shmLCK_ptr[0]);

    pthread_exit(NULL);
}

void* tryFetch2(line_t* intBuff){
    
    /*bloqueo buffer 1 para lectura*/
    pthread_mutex_lock(&shmLCK_ptr[1]);

 
    for(int i=0;i<5;i++){
        strcpy(intBuff[i].line,shmPPB_ptr[i+6].line);
    }


    checksum(2);
    if(cserror == 1){
        printf("\n\t\e[1;31mHa habido un error en la comunicacion (buffer 2).\e[0m\n");
    }

    pthread_mutex_unlock(&shmLCK_ptr[1]);

    pthread_exit(NULL);
}

void* tryCounting(unsigned int* nowBuff){
    for(int i=0;i<9;i++){
        sleep(1);
        printf("."); fflush(stdout);
    }
    pthread_mutex_unlock(&shmLCK_ptr[(*nowBuff)-1]);
    timeOut = 1;
    printf("!\n");


    pthread_exit(NULL);
}




void* tryDisp(line_t* intBuff){
    for(int i=0;i<5;i++){
        printf("%s",intBuff[i].line);
    }

    pthread_exit(NULL);
}

void* tryPoet(line_t* intBuff){
    for(int i=0;i<5;i++){
        fputs(intBuff[i].line, pf);
    }

    pthread_exit(NULL);
}


unsigned int manageTurn(unsigned int atWork){
    if(chTurn){
        printf("\n\t\e[1;31m''''CAMBIO DE TURNO''''\e[0m\n");
        shmPID_ptr[2]=0;
        if(shmPID_ptr[(atWork ^ 0x01)] != 0 ){
            chTurn = 0;
            
            if(shmPID_ptr[atWork] != 0){
                if(kill(shmPID_ptr[atWork],SIGUSR1) == 0){
                    while(!chTurn);
                } else {
                    printf("\e[1;31mEl proceso productor P%d ha terminado inesperadamente\e[0m\n",(atWork+1));
                    shmPID_ptr[atWork] = 0;
                }
            }
            if(kill(shmPID_ptr[atWork ^ 0x1],SIGUSR1) != 0){
                printf("\e[1;31mEl proceso productor P%d ha terminado inesperadamente\e[0m\n",((atWork^1)+1));
                shmPID_ptr[(atWork ^ 0x01)] = 0;
                chTurn = 0;
                if(kill(shmPID_ptr[atWork],SIGUSR1) == 0){ //rewind  
                    atWork = atWork ^ 1;
                } else {
                    printf("\e[1;31mSe procede con la terminacion.\e[0m\n");
                    flagT = 0;
                }
                
            }
            
            sleep(1);   //wait for the other process to get on with it

            atWork = atWork ^ 1;
        } else {
            printf("\n\e[1;31mNo hay otro proceso distinto al P%d para hacer el relevo.\e[0m\n", (atWork+1));

            if(shmPID_ptr[atWork] != 0){
                if(timeOut == 1){
                    flagT = 0;
                    printf("\e[1;31mSe procede con la terminacion.\e[0m\n");
                }else{
                    printf("\e[1;31mEl proceso P%d continua.\e[0m\n",(atWork+1));
                }
            } else {
                printf("\e[1;31mSe procede con la terminacion.\e[0m\n");
                flagT = 0;
            }
        }
        shmPID_ptr[2]=getpid();
        chTurn = 0;
    }

    return atWork;
}



void consTermination(){
    printf("\n\e[1;31m''''TERMINATION PROTOCOL HAS STARTED''''\n");
    shmPID_ptr[2] = 0;
    if((shmPID_ptr[0] != 0)){
        printf("\n\t\e[1;31mSignaling %d for termination...\n",shmPID_ptr[0]);
        flagT = 1;
        if(kill(shmPID_ptr[0],SIGTERM) == 0){
            while(flagT){
                sleep(1);
                if(kill(shmPID_ptr[0],SIGTERM) != 0){
                    break;
                }
            }
        } else {
            printf("El proceso %d ha terminado inesperadamente.",shmPID_ptr[0]);
        }
        
    }
    if (shmPID_ptr[1] != 0){
        printf("\n\t\e[1;31mSignaling %d for termination...\n",shmPID_ptr[1]);
        flagT = 1;
        if(kill(shmPID_ptr[1],SIGTERM) == 0){
            while(flagT){
                sleep(1);
                if(kill(shmPID_ptr[1],SIGTERM) != 0){
                    break;
                }
            }
        } else {
            printf("El proceso %d ha terminado inesperadamente.",shmPID_ptr[1]);
        }
    }

    
    //dettaching
    pthread_mutex_destroy(&shmLCK_ptr[0]);
    pthread_mutex_destroy(&shmLCK_ptr[1]);
    munmap((void *)shmLCK_ptr, 2*sizeof(pthread_mutex_t));
    shmLCK_ptr = NULL;
    
    shmdt((const void*) shmPPB_ptr);
    shmdt((const void*) shmPID_ptr);
    //dettaching

    //freeing
    shmctl(shmPPB_id, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(shmPID_id, IPC_RMID, (struct shmid_ds *)NULL);
    close(FD);
    shm_unlink("mutex");
    FD = 0;
    fclose(pf);
    printf("\n\e[1;31mExiting.\n\n");
    return;

}


void checksum(int nowBuff){
    line_t csreg;
    for(int i=0;i<200;i++){
        csreg.line[i] = 0;
        for(int j=0;j<5;j++){
            csreg.line[i] += shmPPB_ptr[j+(6*(nowBuff-1))].line[i];
        }
        csreg.line[i] = csreg.line[i] ^ 0xFF;
        if(csreg.line[i] != shmPPB_ptr[(nowBuff)*6-1].line[i]){
            cserror = 1;
            return;
        } else {
            shmPPB_ptr[(nowBuff)*6-1].line[i] += 1;
            cserror = 0;
        }
    }
    
    return;
}






//TO-DO

/*
 - chequeo de existencia
 - threading the printers


*/