#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h> 
#include <sys/mman.h>
#include <fcntl.h>
#include <ctype.h>

#define TAM_buffer 10
#define SIG_BLOCK 0    // Valor para bloquear señales
#define SIG_UNBLOCK 1  // Valor para desbloquear señales
#define BUFFER_1 1
#define BUFFER_2 2

void termination_prod();
void term_prod_like_cons();
void handler_SIGTERM(int);
void handler_SIGUSR1(int);
void* writeB1(int* arg);
void* writeB2(int* arg);
void checksum(int);

int flagT = 1;
int myturn= 0;

typedef struct{
    char line[200];
} line_t;

char line[200];
line_t *shmPPB_ptr = NULL;
pthread_mutex_t *mutex_ptr = NULL;
pid_t *shmPID_ptr = NULL;
int * shmPFcursor_ptr = NULL;

FILE* pf;
int fd;
int shmPPB_id;
int shmPF_id;
int shmLCK_id;
int shmPID_id;
pid_t PID_cons;
pid_t PID_prod;
struct shmid_ds mem_data_prods;
struct shmid_ds mem_data_PID;
char prod;

int main(){
    key_t key;
    
    sigset_t block_set;
    

    pthread_t tid[2];
    signal(SIGTERM, handler_SIGTERM);
    signal(SIGUSR1, handler_SIGUSR1);

    /*Inicializa el conjunto de señales a bloquear*/
    sigemptyset(&block_set);
    sigaddset(&block_set, SIGUSR1);
    sigaddset(&block_set, SIGTERM);

    int error;

    

    printf("PID productor: %d\n",getpid());

    if((pf=fopen("quijoteIN.txt", "r")) == NULL){
        printf("Error al abrir el archivo\n");
        exit(1);
    }

    ///////////////////PING PONG BUFFER//////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    key = ftok("/bin/ls",137);
    if (key == -1){
        printf("No consegui clave para mc del BUFFER.\n");
        exit(1);
    }
    /*creacion del espacio de memoria compartida*/
    shmPPB_id = shmget(key, (TAM_buffer+2)*sizeof(line_t), 0666);
    if (shmPPB_id == -1){
        printf("No consegui ID para mc del BUFFER\n");
        exit (2);
    }
    /*Asocicion de un puntero al espacio de memoria*/
    shmPPB_ptr = (line_t *)shmat(shmPPB_id, (const void *)0, 0);
    if (shmPPB_ptr == NULL){
        printf("No consegui asociar la mc del BUFFER a una variable\n");
        exit (3);
    }


    ///////////////////LOCKS/////////////////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    if((fd = shm_open("mutex",O_RDWR, 0660)) == -1){
        printf("No consegui MC para el mutex\n");
        exit(6);
    }
    if (ftruncate(fd, 2*sizeof(pthread_mutex_t)) != 0) {
        printf("No consegui truncar el espacio de MC para el mutex\n");
        exit(7);
    }

    void *addr = mmap(NULL,2*sizeof(pthread_mutex_t), PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
    if (addr == MAP_FAILED) {
        printf("No consegui mapear la MC para el mutex\n");
        exit(8);
    }

    mutex_ptr = (pthread_mutex_t *)addr;

    ///////////////////PF cursor y BUFFER//////////////////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    key = ftok("/bin/ps",32);
    if (key == -1){
        printf("No consegui clave para mc del PF y buffer\n");
        exit(9);
    }
    /*creacion del espacio de memoria compartida*/
    shmPF_id = shmget(key, 2*sizeof(int), 0666| IPC_CREAT);
    if (shmPF_id == -1){
        printf("No consegui ID para mc del PF y buffer\n");
        exit (10);
    }
    /*Asocicion de un puntero al espacio de memoria*/
    shmPFcursor_ptr = (int *)shmat(shmPF_id, (const void *)0, 0);
    if (shmPFcursor_ptr == NULL){
        printf("No consegui asociar la mc del PF y buffer a una variable\n");
        exit (11);
    }

    ///////////////////PIDs//////////////////////////////////////////////////
    /*creacion de clave para identificar a la porcion de memoria compartida*/
    key = ftok("/bin/clear",65);
    if (key == -1){
        printf("No consegui clave para mc de los PIDs\n");
        exit(9);
    }
    /*creacion del espacio de memoria compartida*/
    shmPID_id = shmget(key, 3*sizeof(pid_t), 0666);
    if (shmPID_id == -1){
        printf("No consegui ID para mc de los PIDs\n");
        exit (10);
    }
    /*Asocicion de un puntero al espacio de memoria*/
    shmPID_ptr = (pid_t *)shmat(shmPID_id, (const void *)0, 0);
    if (shmPID_ptr == NULL){
        printf("No consegui asociar la mc de los PIDs a una variable\n");
        exit (11);
    }

    char share_PID;
    int buf;
    shmctl(shmPID_id, IPC_STAT, &mem_data_PID);
    if(mem_data_PID.shm_nattch == 2){
        /*soy el productor 1 y arranco yo*/
        prod=1;
        myturn=1;
        share_PID=1;
        buf=BUFFER_1;
        printf("\n\t ''''EMPIEZA MI TURNO''''\n");
    }else if(mem_data_PID.shm_nattch == 3){
        sleep(6);//espero a que todo se estabilice.
        if(shmPID_ptr[0] == 0 ){
            /*soy el "productor 1" pero no arranco yo*/
            prod=1;
            shmPID_ptr[0] = getpid();
            share_PID=0;
        }else if(shmPID_ptr[1] == 0 ){
            /*soy el "productor 2"*/
            prod=2;
            myturn=0;
            shmPID_ptr[1] = getpid();
            share_PID=0;
        }else{
            /*soy el productor 2 y espero mi turno*/
            prod=2;
            myturn=0;
            shmPID_ptr[1] = getpid();
            share_PID=0;
        }
    }else if(mem_data_PID.shm_nattch > 3){
        printf("Soy un proceso intruso. Procedo a terminar.\n");
        /*dettaching*/
        shmdt((const void *) shmPPB_ptr);
        munmap((void *)mutex_ptr, sizeof(pthread_mutex_t)); 
        mutex_ptr = NULL;
        fd = 0;
        shmdt((const void *) shmPID_ptr);
        shmdt((const void *) shmPFcursor_ptr);
        fclose(pf);
        exit(12);
    }

    int ans=1;
    PID_cons=shmPID_ptr[2];
    while( flagT & ans){
        /*actualizo el PID del otro productor*/
        PID_prod=shmPID_ptr[(prod^3)-1];
        shmctl(shmPID_id, IPC_STAT, &mem_data_PID);
        shmctl(shmPF_id, IPC_STAT, &mem_data_prods);
        if(mem_data_PID.shm_nattch <= mem_data_prods.shm_nattch){
            myturn=0;
            term_prod_like_cons();
            exit(-1);
        }
        if (myturn & ans & (buf == BUFFER_1) & flagT){
            /*comparto la posicion del cursor*/
            shmPFcursor_ptr[0]= ftell(pf);
            /*comparto buffer en el que se debe escribir en la proxima iteracion*/
            shmPFcursor_ptr[1]= BUFFER_2;


            /*Bloquea las señales SIGUSR1 y SIGTERM*/
            sigprocmask(SIG_BLOCK, &block_set, NULL);
            /*el hilo 0 lee del archivo y escribe en el buffer 1*/
            error = pthread_create(&(tid[0]), NULL, (void*)writeB1,(void*)&ans); 
            if (error != 0) {
                printf("\nThread can't be created :[%s]", strerror(error));/////////
            }
            int *res;
            pthread_join(tid[0], (void*) &res);
            ans=*res;
            
            /*Desbloquea las señales SIGUSR1 y SIGTERM*/
            sigprocmask(SIG_UNBLOCK, &block_set, NULL);

            if(share_PID){
                /*comparto mi PID para disparar la lectura y para realizar los cambios de turno*/
                if(prod == 1){
                    shmPID_ptr[0] = getpid();
                    printf("\tPID productor 1 : %d\n",shmPID_ptr[0]);
                }
                share_PID=0;
            }

            if(ans==0){
                char car;
                printf("Desea reiniciar la lectura del archivo(S/N):");
                scanf(" %c", &car);
                if (car == 's' || car == 'S'){
                    ans=1;
                    fseek(pf, 0, SEEK_SET);
                }
            }

            /*si sigue siendo mi turno paso al otro buffer*/
            buf=BUFFER_2;
            if(myturn==0){
                printf("\n\t ''''TERMINA MI TURNO''''\n");
                /*comparto la posicion del cursor*/
                shmPFcursor_ptr[0]= ftell(pf);
                /*Envio un eco*/
                kill(PID_cons,SIGUSR1);
            }

        }else if(myturn & ans & (buf == BUFFER_2) & flagT){
            
            /*comparto la posicion del cursor*/
            shmPFcursor_ptr[0]= ftell(pf);
            /*comparto buffer en el que se debe escribir en la proxima iteracion*/
            shmPFcursor_ptr[1]= BUFFER_1;


            /*Bloquea las señales SIGUSR1 y SIGTERM*/
            sigprocmask(SIG_BLOCK, &block_set, NULL);
            
            /*el hilo 0 lee del archivo y escribe en el buffer 1*/
            error = pthread_create(&(tid[1]), NULL, (void*)writeB2,(void*)&ans); 
            if (error != 0) {
                printf("\nThread can't be created :[%s]", strerror(error));/////////
            }
            int *res;
            pthread_join(tid[1], (void*) &res);
            ans=*res;
            
            /*Desbloquea las señales SIGUSR1 y SIGTERM*/
            sigprocmask(SIG_UNBLOCK, &block_set, NULL);

            if(ans==0){
                char car;
                printf("Desea reiniciar la lectura del archivo(S/N):");
                scanf(" %c", &car);
                if (car == 's' || car == 'S'){
                    ans=1;
                    fseek(pf, 0, SEEK_SET);
                }
            }

            /*si sigue siendo mi turno paso al otro buffer*/
            buf=BUFFER_1;
            if(myturn==0){
                printf("\n\t ''''TERMINA MI TURNO''''\n");
                /*comparto la posicion del cursor*/
                shmPFcursor_ptr[0]= ftell(pf);
                /*Envio un eco*/
                kill(PID_cons,SIGUSR1);
            }
        }else if (myturn == 0){
            while(myturn == 0 & flagT){
            pause();
            }
            if(myturn){
                /*acomodo la ubicacion del cursor*/
                fseek(pf,shmPFcursor_ptr[0],SEEK_SET);
                /*me preparo para escribir el buffer que corresponda*/
                buf=shmPFcursor_ptr[1];
                printf("\n\t ''''EMPIEZA MI TURNO''''\n");
            }
        }
        if(flagT == 0){
            printf("\n\t ''''TERMINA MI TURNO''''\n");
            /*comparto la posicion del cursor*/
            shmPFcursor_ptr[0]= ftell(pf);
            /*comparto buffer en el que se debe escribir*/
            shmPFcursor_ptr[1]= buf;

            shmctl(shmPID_id, IPC_STAT, &mem_data_PID);
            if(mem_data_PID.shm_nattch == 1){
                term_prod_like_cons();
                exit(-1);
            }
        }
    }
    
    termination_prod();
    exit(0);
}

void handler_SIGTERM(int sig){
    flagT=0;
}

void handler_SIGUSR1(int sig){
    if(shmPID_ptr[2]==0){
        myturn^=1;
    }  
}


void* writeB1(int* arg){
    
    printf("soy el hilo 1, esperando para escribir el buffer 1..\n");

    /*bloqueo buffer 1*/
    pthread_mutex_lock(&mutex_ptr[0]); 

    /*leo del archivo y escribo en el buffer*/
    printf("soy el hilo 1 escribiendo el buffer 1\n");

    int i=0;
    while(i<TAM_buffer/2){
        fgets(line, 200, pf);

        if (feof(pf)) {
            printf("\tFin del archivo alcanzado.\n");
            *arg=0;
            pthread_exit((void *)arg);
        }

        /*solo escribo en el buffer las lineas del archivo que contegan texto*/
        int white_space=0;
        int tam=strlen(line);
        for (int j = 0; j < tam; j++) { 
            if (isspace((unsigned char)line[j])){
                white_space++;
            }
        }
        if (white_space < tam){
            if(prod == 1){
                strcpy(shmPPB_ptr[i].line,"P1: ");
            }else if (prod == 2){
                strcpy(shmPPB_ptr[i].line,"P2: ");
            }
            strcat(shmPPB_ptr[i].line,line);
            printf("buffer 1[%d], escrito por %d = %s\n",i,getpid(),shmPPB_ptr[i].line);
            sleep(1);
            i++;
        }
    }

    checksum(1);
      
    /*desbloqueo el buffer 1 para escritura*/
    pthread_mutex_unlock(&mutex_ptr[0]); 

    printf("soy el hilo 1 y ya escribi el buffer 1\n");
    
    *arg=1;
    pthread_exit((void *)arg);
}

void* writeB2(int* arg){
    
    printf("soy el hilo 2, esperando para escribir el buffer 2..\n");
    
    /*bloqueo buffer 2*/
    pthread_mutex_lock(&mutex_ptr[1]); 

    /*leo del archivo y escribo en el buffer*/
    printf("soy el hilo 2 escribiendo el buffer 2\n");
    
    int i=(TAM_buffer/2)+1; 
    while(i<(TAM_buffer+1)){
        fgets(line, 200, pf);

        if (feof(pf)) {
            printf("\tFin del archivo alcanzado.\n");
            *arg=0;
            pthread_exit((void *)arg);
        }

        /*solo escribo en el buffer las lineas del archivo que contegan texto*/
        int white_space=0;
        int tam=strlen(line);
        for (int j = 0; j < tam; j++) { 
            if (isspace((unsigned char)line[j])){
                white_space++;
            }
        }
        if (white_space < tam){
            if(prod == 1){
                strcpy(shmPPB_ptr[i].line,"P1: ");
            }else if (prod == 2){
                strcpy(shmPPB_ptr[i].line,"P2: ");
            }
            strcat(shmPPB_ptr[i].line,line);
            printf("buffer 2[%d] escrito por %d = %s\n",i,getpid(),shmPPB_ptr[i].line);
            sleep(1);
            i++;
        }
    }

    checksum(2);

    /*desbloqueo el buffer 2 para escritura*/
    pthread_mutex_unlock(&mutex_ptr[1]); 

    printf("soy el hilo 2 y ya escribi el buffer 2\n");
    
    *arg=1;
    pthread_exit((void *)arg);
}

void termination_prod(){

    if(shmPID_ptr[2]==0){
        /*Protocolo de terminación normal*/

        /*dettaching*/
        shmdt((const void *) shmPPB_ptr);
        munmap((void *)mutex_ptr, sizeof(pthread_mutex_t)); 
        mutex_ptr = NULL;
        fd = 0;
        shmdt((const void *) shmPID_ptr);
        fclose(pf);

        shmctl(shmPF_id, IPC_STAT, &mem_data_prods);
        if(mem_data_prods.shm_nattch > 1){
            shmdt((const void *) shmPFcursor_ptr);
        }else{
            shmdt((const void *) shmPFcursor_ptr);
            shmctl(shmPF_id,IPC_RMID,(struct shmid_ds*)NULL);
        }
        printf("Procediendo a terminar el proceso productor %d\n",prod);
        kill(PID_cons,SIGTERM);//Envio un eco
        return;
    }else{

        /*Protocolo de terminación alternativo*/
        shmPID_ptr[prod-1]=0;

        if(myturn){
            kill(PID_cons,SIGUSR1);// le aviso al consumidor para que cambie de turno
        }
        /*dettaching*/
        shmdt((const void *) shmPPB_ptr);
        munmap((void *)mutex_ptr, sizeof(pthread_mutex_t)); 
        mutex_ptr = NULL;
        fd = 0;
        shmctl(shmPF_id, IPC_STAT, &mem_data_prods);
        
        if(mem_data_prods.shm_nattch > 1){
            shmdt((const void *) shmPFcursor_ptr);
            if (shmPID_ptr[(prod^3)-1] == 0){
                kill(PID_prod,SIGTERM);
            }
        }else{
            shmdt((const void *) shmPFcursor_ptr);
            shmctl(shmPF_id,IPC_RMID,(struct shmid_ds*)NULL);
        }
        fclose(pf);
        shmdt((const void *) shmPID_ptr);

        printf("Procediendo a terminar el proceso productor %d\n",prod);
        return;
    } 
}

void term_prod_like_cons(){
    printf("\n\e[1;31m''''EMERGENCY TERMINATION PROTOCOL HAS STARTED''''\n");
    shmPID_ptr[prod-1]=0;
    if((shmPID_ptr[(prod^3)-1] != 0)){
        printf("\n\t\e[1;31mSignaling %d for termination...\n",shmPID_ptr[(prod^3)-1]);
        flagT = 1;
        if(kill(shmPID_ptr[(prod^3)-1],SIGTERM) == 0){
            while(flagT){
                sleep(1);
                if(kill(shmPID_ptr[(prod^3)-1],SIGTERM) != 0){
                    break;
                }
            }
        }    
    }else {
        printf("El proceso %d ha terminado inesperadamente.",shmPID_ptr[(prod^3)]);
    }

    //dettaching
    pthread_mutex_destroy(&mutex_ptr[0]);
    pthread_mutex_destroy(&mutex_ptr[1]);
    munmap((void *)mutex_ptr, 2*sizeof(pthread_mutex_t));
    mutex_ptr = NULL;
    
    shmdt((const void*) shmPPB_ptr);
    shmdt((const void*) shmPID_ptr);
    shmdt((const void *) shmPFcursor_ptr);
            
    //dettaching

    //freeing
    shmctl(shmPPB_id, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(shmPID_id, IPC_RMID, (struct shmid_ds *)NULL);
    shmctl(shmPF_id,IPC_RMID,(struct shmid_ds*)NULL);
    close(fd);
    shm_unlink("mutex");
    fd = 0;
    fclose(pf);
    printf("\n\e[1;31mExiting.\n\n");
    return;
}

void checksum(int nowBuff){
    for(int i=0;i<200;i++){
        shmPPB_ptr[(nowBuff)*6-1].line[i] = 0;
        for(int j=0;j<TAM_buffer/2;j++){
            shmPPB_ptr[(nowBuff)*6-1].line[i] += shmPPB_ptr[j+(6*(nowBuff-1))].line[i];
        }
        shmPPB_ptr[(nowBuff)*6-1].line[i] = shmPPB_ptr[(nowBuff)*6-1].line[i] ^ 0xFF;
    }
    return;
}