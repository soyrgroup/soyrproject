#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Uso: %s <W/w para escritor o R/r para lector>\n", argv[0]);
        exit(1);
    }

    // Proceso escritor
    if (strcmp(argv[1], "W") == 0 || strcmp(argv[1], "w") == 0) {
        const char *fifoName = "intercambio"; // Nombre del FIFO
        int fd_entrada; // Descriptores de archivo
        ssize_t bytes_ent;
        void *buf = NULL;

        mkfifo(fifoName, 0666); // Crea el FIFO

        int fd = open(fifoName, O_CREAT | O_WRONLY, 0666); // Abre el FIFO para escritura

        if (fd == -1) {
            perror("Error al abrir el FIFO");
            exit(EXIT_FAILURE);
        }
        
        /*se abre el archiv de texto*/
        fd_entrada = open("pg2000.txt", O_RDONLY);

        if (fd_entrada == -1) {
            perror("Error al abrir el archivo de entrada");
            exit(EXIT_FAILURE);
        }

        /*Intenta leer el archivo en trozos de 1024 bytes*/ 
        size_t chunk_size = 1024;
        size_t total_size = 0;

        /*Asigna memoria al búfer antes de la lectura*/ 
        buf = malloc(chunk_size);
        if (buf == NULL) {
            perror("Error al asignar memoria para el búfer");
            exit(EXIT_FAILURE);
        }

        while ((bytes_ent = read(fd_entrada, buf + total_size, chunk_size)) > 0) {
            total_size += bytes_ent;
            buf = realloc(buf, total_size + chunk_size); // Ajusta el tamaño del búfer
            if (buf == NULL) {
                perror("Error al asignar memoria para el búfer");
                exit(EXIT_FAILURE);
            }
        }

        if (bytes_ent == -1) {
            perror("Error al leer el archivo de entrada");
            exit(EXIT_FAILURE);
        }

        // Cierra el archivo de entrada después de su uso
        if (close(fd_entrada) == -1) {
            perror("Error al cerrar el archivo de entrada");
            exit(EXIT_FAILURE);
        }
        
        write(fd, buf,total_size); // Escribe el mensaje en el FIFO

        free(buf); // Libera la memoria asignada para el búfer

        if (close(fd) == -1) {
            perror("Error al cerrar el pipe");
            exit(EXIT_FAILURE);
        }
        exit(0);
    }

    // Proceso lector
    else if (strcmp(argv[1], "R") == 0 || strcmp(argv[1], "r") == 0) {
        const char *fifoName = "intercambio"; // Nombre del FIFO
        int fd_sal; // Descriptores de archivo
        ssize_t bytes_sal;
        void *buf = NULL;

        int fd = open(fifoName, O_RDONLY); // Abre el FIFO para lectura

        if (fd == -1) {
            perror("Error al abrir el FIFO");
            exit(EXIT_FAILURE);
        }

        /*Intenta leer el archivo en trozos de 1024 bytes*/ 
        size_t chunk_size = 1024;
        size_t total_size = 0;

        /*Asigna memoria al búfer antes de la lectura*/ 
        buf = malloc(chunk_size);
        if (buf == NULL) {
            perror("Error al asignar memoria para el búfer");
            exit(EXIT_FAILURE);
        }

        while ((bytes_sal = read(fd, buf + total_size, chunk_size)) > 0) {
            total_size += bytes_sal;
            buf = realloc(buf, total_size + chunk_size); // Ajusta el tamaño del búfer
            if (buf == NULL) {
                perror("Error al asignar memoria para el búfer");
                exit(EXIT_FAILURE);
            }
        }

        if (bytes_sal == -1) {
            perror("Error al leer el pipe");
            exit(EXIT_FAILURE);
        }

        if (close(fd) == -1) {
            perror("Error al cerrar el pipe");
            exit(EXIT_FAILURE);
        }

        fd_sal = open("salida.txt", O_CREAT | O_WRONLY | O_TRUNC, 0666);

        if (fd_sal == -1) {
            perror("Error al abrir el archivo de salida");
            exit(EXIT_FAILURE);
        }

        bytes_sal = write(fd_sal, buf, total_size);

        if (bytes_sal == -1) {
            perror("Error al escribir el archivo de salida");
            exit(EXIT_FAILURE);
        }

        free(buf); // Libera la memoria asignada para el búfer

        // Cierra el archivo de salida después de su uso
        if (close(fd_sal) == -1) {
            perror("Error al cerrar el archivo de salida");
            exit(EXIT_FAILURE);
        }

        exit(0);
    }

    // Opción no reconocida
    else {
        printf("Identificador de proceso no reconocido\n");
        exit(4);
    }
}
