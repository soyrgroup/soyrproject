#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>

pthread_mutex_t *mutex_ptr = NULL;

void* tryThis(void* arg){

    printf("hey!\n"); 
	pthread_mutex_lock(mutex_ptr); 

	printf("\n Job has started\n"); 

	for (unsigned long i = 0; i < (0xFFFFFFFF); i++);
    
    printf("\n Job has finished\n"); 

	pthread_mutex_unlock(mutex_ptr); 

	return NULL;
} 



int main(){
    key_t key;
    pthread_t tid;
    pthread_mutexattr_t att;

    
    printf("size is %ld", sizeof(pthread_mutex_t));

    int fd;
    if((fd = shm_open("mutex",O_RDWR, 0660)) == -1){
        exit(1);
    }
    if (ftruncate(fd, sizeof(pthread_mutex_t)) != 0) {
        exit(2);
    }

    void *addr = mmap(NULL, sizeof(pthread_mutex_t), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        exit(3);
    }

    mutex_ptr = (pthread_mutex_t *)addr;


    pthread_create(&tid, NULL, (void* )tryThis, NULL);


    pthread_join(tid, NULL);

    //dettaching
    munmap((void *)mutex_ptr, sizeof(pthread_mutex_t));
    mutex_ptr = NULL;
    fd = 0;
    //dettaching

    return 0;
}