#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>  // Para la función open
#include <unistd.h> // Para la función close
#include <string.h>
#include <time.h>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Uso: %s -v <archivo_entrada> <archivo_salida>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[1], "-v") == 0) {
        
        // Registrar el tiempo al inicio del programa
        clock_t start_time = clock();
        clock_t end_time;
        double total_time;
        char *opcion = argv[1]; // opcion para mostrar 
        char *archivo_entrada = argv[2]; // Ruta del archivo de entrada
        char *archivo_salida = argv[3];  // Ruta del archivo de salida

        int fd_entrada, fd_salida; // Descriptores de archivo
        ssize_t bytes_ent;
        ssize_t bytes_salida;
        void *buf = NULL;

        fd_entrada = open(archivo_entrada, O_RDONLY);

        if (fd_entrada == -1) {
            perror("Error al abrir el archivo de entrada");
            exit(EXIT_FAILURE);
        }

        // Intenta leer el archivo en trozos de 1024 bytes
        size_t chunk_size = 1024;
        size_t total_size = 0;

        // Asigna memoria al búfer antes de la lectura
        buf = malloc(chunk_size);
        if (buf == NULL) {
            perror("Error al asignar memoria para el búfer");
            exit(EXIT_FAILURE);
        }

        while ((bytes_ent = read(fd_entrada, buf + total_size, chunk_size)) > 0) {
            total_size += bytes_ent;
            buf = realloc(buf, total_size + chunk_size); // Ajusta el tamaño del búfer
            if (buf == NULL) {
                perror("Error al asignar memoria para el búfer");
                exit(EXIT_FAILURE);
            }
        }

        if (bytes_ent == -1) {
            perror("Error al leer el archivo de entrada");
            exit(EXIT_FAILURE);
        }

        // Cierra el archivo de entrada después de su uso
        if (close(fd_entrada) == -1) {
            perror("Error al cerrar el archivo de entrada");
            exit(EXIT_FAILURE);
        }

        fd_salida = open(archivo_salida, O_CREAT | O_WRONLY | O_TRUNC, 0666);

        if (fd_salida == -1) {
            perror("Error al abrir el archivo de salida");
            exit(EXIT_FAILURE);
        }

        bytes_salida = write(fd_salida, buf, total_size);

        if (bytes_salida == -1) {
            perror("Error al escribir el archivo de salida");
            exit(EXIT_FAILURE);
        }

        free(buf); // Libera la memoria asignada para el búfer

        // Cierra el archivo de salida después de su uso
        if (close(fd_salida) == -1) {
            perror("Error al cerrar el archivo de salida");
            exit(EXIT_FAILURE);
        }

        end_time = clock();

        total_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        
        //imprimo los detalles de la operacion
        printf("Descriptor de archivo origen: %d\n", fd_entrada);
        printf("Descriptor de archivo destino: %d\n", fd_salida);
        printf("Cantidad de bytes leídos en el origen: %d\n", (int)total_size);
        printf("Cantidad de bytes escritos en el destino: %d\n", (int)bytes_salida);
        printf("Tiempo total de ejecución: %.2f segundos\n", total_time);
        return 0;
    } else {
        printf("Opción incorrecta\n");
        return -1;
    }
    return -2;
}



