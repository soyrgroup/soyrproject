#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define NUM 18
#define ARCHIVO "/bin/ls"

int main( int argc, char *argv[])
{
	key_t key;
	int mem_id;
    int *shm_ptrB = NULL;
	struct shmid_ds mem_data;
	int status;

	if (strcmp(argv[1],"b")== 0  || strcmp(argv[1],"B") == 0){
        printf("ejecutando el proceso %s \n",argv[1]);

		/*creacion de clave para identificar a la porcion de memoria compartida*/
		key = ftok (ARCHIVO,NUM);
		if (key == -1){
			printf("No consegui  clave para memoria compartida\n");
			exit(1);
		}

		/*creacion del espacio de memoria compartida*/
		mem_id = shmget (key, 20*sizeof(int), 0666);
		if (mem_id == -1){
			printf("No consegui Id para memoria compartida\n");
			exit (2);
		}

		/*Asocicion de un puntero al espacio de memoria*/
		shm_ptrB = (int *)shmat (mem_id, (const void *)0, 0);
		if (shm_ptrB == NULL){
			printf("No consegui asociar la memoria compartida a una variable\n");
			exit (3);
		}

		for (int j=10; j<20; j++){
			shm_ptrB[j] = j+2;
			printf("Proceso %d: Memoria[%d]= %d\n",getpid(),j,shm_ptrB[j]);
			sleep (1);
		}

		shmctl (mem_id, IPC_STAT, &mem_data);

		if(mem_data.shm_cpid == getpid()){
			printf("el proceso B fue el creador\n");
			printf("esperando a que el proceso A termine de escribir...\n");
			waitpid(mem_data.shm_lpid, &status, 0);
		}
		else{
			printf("el proceso A fue el creador");
		}

		for (int j=0; j<10; j++){
			printf("Proceso %d: Leido desde Memoria[%d]= %d\n",getpid(),j, shm_ptrB[j]);
			sleep (1);
		}

    	shmdt ((const void *) shm_ptrB);
		
		exit(0);
	}

	else{
        printf("identificador de proceso no reconocido\n");
        exit(4);
    }  
}