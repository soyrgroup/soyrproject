#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define NUMERO 123
#define ARCHIVO "/bin/more"
int main( int argc, char **argv)
{
	key_t Clave;
	int Id_Memoria1;
	long *Memoria1 = NULL;
	int i,j;

    /*-------------------------------------------------------------------------
    Antes que nada debo conseguir una clave unica. Voy a usar el archivo 
    /bin/more  y un numero que voy a definir con las Macros ARCHIVO y NUMERO
    -------------------------------------------------------------------------*/
	Clave = ftok (ARCHIVO, NUMERO);
	if (Clave == -1)
	{
		printf("No consegui  clave para memoria compartida\n");
		exit(1);
	}

	/* ----------------------------------------------------------------------
    Ahora tengo que solicitarle al Sistema Operativo que otorgue la memoria
    para compartir. 
    uso la llamada a sistema shmget() a la que tengo que pasarle:
    Clave
    Tamaño de memoria necesaria (en este caso voy a solicitar espacio 
    para 50 long) 
    Flags: los ultimos nueve bits corresponden a permisos en octal (rwxrwxrwx)
    para el dueño, el grupo y el resto del mundo respectivamente). 
    
    A eso hay que agregar otros flags que indican como se va a usar, principalmente
    IPC_CREAT
    vamos a obtener un identificador del bloque de memoria creado 
    o -1 si hubo un error.
    -----------------------------------------------------------------------*/
	Id_Memoria1 = shmget (Clave, 50*sizeof(long), 0666 | IPC_CREAT);
	if (Id_Memoria1 == -1)
	{
		printf("No consegui Id para memoria compartida\n");
		exit (2);
	}
    /* ----------------------------------------------------------------------
	Ahora debo asociar la identificación de bloque de memoria con una 
    variable del programa, para eso uso la llamada al sistema shmat()

    void *shmat(int shmid, const void *shmaddr, int shmflg);

    Para ver otras posibilidades de flags, ver man shmat
    ----------------------------------------------------------------------*/
	Memoria1 = (long *)shmat (Id_Memoria1, (const void *)0, 0);
     
	if (Memoria1 == NULL)
	{
		printf("No consegui asociar la memoria compartida a una variable\n");
		exit (3);
	}

	 /* ----------------------------------------------------------------------
	Ya podemos usar la memoria compartida como una variable mas.
    en este caso vamos a escribir en la memoria los numeros pares del 0 al 98
    a razon de 1 por segundo
    ----------------------------------------------------------------------*/
	
		for (j=0; j<50; j++)
		{
		Memoria1[j] = j*2;
		printf("Proceso %d: Memoria1[%d]= %ld\n",getpid(),j,Memoria1 [j]);
		sleep (1);
		}
 
	

    /* ----------------------------------------------------------------------
	Terminada de usar la memoria compartida, la liberamos.
    Esto se hace en dos pasos, 
    Primero se elimina la asociación entre el bloque de memoria y el puntero 
    (variable del programa) con shmdt()

    Luego se indica al sistema operativo que libere el bloque de memoria compartida 
    El sistema toma nota del pedido y lo va a liberar cuando el ultimo programa 
    usuario deje de usarlo
    Esto se hace con 

    int shmctl(int shmid, int cmd, struct shmid_ds *buf);
    donde shmid es la identificación que obtuvimos con shmget
    cmd puede tomar distintos valores, pero para liberar la memoria es IPC_RMID
    struct shmid_ds buf es una estructura que contienen información acerca del bloque 
    de memoria. cuando cmd es IPC_RMID se ignora

    ----------------------------------------------------------------------*/
	
	
	shmdt ((const void *) Memoria1);

	shmctl (Id_Memoria1, IPC_RMID, (struct shmid_ds *)NULL);

/* a partir de aca terminamos el programa*/
exit(0);
}
