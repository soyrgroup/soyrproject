# Ejercicio 6: Memoria Compartida

El archivo ej6signal.c contiene un programa que puede actuar como dos procesos distintivos. La instrucciones que se ejecuten dependen directamente del parámetro que se pase como argumento. Dichos procesos, A y B, comparten un espacio de memoria por donde se pasan información.

 - El proceso A escribe en los primeros 10 espacios de la memoria compartida y lee los siguientes 10. Esto se repite hasta que algún evento termine su ejecución.

 - El proceso B lee de los primeros 10 espacios de la memoria compartida y escribe en los siguientes 10.  Esto se repite hasta que algún evento termine su ejecución.

## Ejecución del programa

En primera instancia se debe compilar el código:

```bash
gcc -o <nombre del ejecutable> ./ej6signal.c
```

Luego de haber compilado, se puede ejecutar el programa A o B. Se puede ejecutar el programa A con el siguiente comando:
```bash
./<nombre del ejecutable> A
```
De la misma forma, el probrama B se puede ejecutar con el comando:
```bash
./<nombre del ejecutable> B
```

## Terminación normal del programa

 Para terminar los programas se recomienda enviar la señal SIGTERM desde una tercera consola, utilizando el siguiente comando:

```bash
kill -s SIGTERM <PID del proceso A>
```
