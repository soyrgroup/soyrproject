#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM 18
#define ARCHIVO "/bin/ls"

void handler_SIGTERM(int);
void handler_SIGUSR1(int);
void handler_SIGUSR2(int);
int char2int(char,int,int);
char int2char(int);
void terminationA(int* shm_ptrA, pid_t PID_b, int mem_id);
void terminationB(int* shm_ptrB,pid_t PID_a, int mem_id);

/*variables globales*/
char canRead=0;
char canWrite=0;
char flagT=1;

int main( int argc, char *argv[])
{
    /* manejador de señales para SIGTERM*/
    signal(SIGTERM, handler_SIGTERM);
    signal(SIGUSR1, handler_SIGUSR1);
    signal(SIGUSR2, handler_SIGUSR2);

	key_t key;
	int mem_id;
    int a;
    int b;

    //Proceso A
    if (strcmp(argv[1],"a") == 0  || strcmp(argv[1],"A") == 0){
        printf("ejecutando el proceso A de pid: %d \n",getpid());

        int *shm_ptrA = NULL;
        char canRead_A;
        char cad[]={"hola mundo"}; 
        struct shmid_ds mem_data;
        pid_t PID_b;
        a = 4;
        b = 5;

        /*creacion de clave para identificar a la porcion de memoria compartida*/
        key = ftok(ARCHIVO,NUM);
        if (key == -1){
            printf("No consegui clave para memoria compartida\n");
            exit(1);
        }

        /*creacion del espacio de memoria compartida*/
        mem_id = shmget(key, 22*sizeof(int), 0666 | IPC_CREAT);
        if (mem_id == -1){
            printf("No consegui ID para memoria compartida\n");
            exit (2);
        }

        /*Asocicion de un puntero al espacio de memoria*/
        shm_ptrA = (int *)shmat(mem_id, (const void *)0, 0);
        if (shm_ptrA == NULL){
            printf("No consegui asociar la memoria compartida a una variable\n");
            exit (3);
        }

        /*comienzo escribiendo las claves publicas a y b*/
        shm_ptrA[20] = a;
        shm_ptrA[21] = b;

        /*escribo la porcion de memoria que me corresponde*/
        for (int j=0; j<10; j++){
            shm_ptrA[j] = char2int(cad[j],a,b);
            printf("Proceso A con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrA[j]);
            sleep (1);
        }
        printf("\n");
        
        /*verifico que el proceso B se haya ejecutado*/
        shmctl(mem_id, IPC_STAT, &mem_data);
        if(mem_data.shm_nattch == 1){
            printf("esperando a que se ejecute el proceso B ...\n");
            while(mem_data.shm_nattch<2){
                shmctl(mem_id, IPC_STAT, &mem_data);
                if(flagT == 0){
                    shmdt ((const void *) shm_ptrA);
                    printf("El proceso B no existe y se procede a terminar A ...\n");
                    shmctl (mem_id, IPC_RMID, (struct shmid_ds *)NULL);
                    exit(5);
                } 
            }
            printf("el proceso A fue el creador\n");
            PID_b=mem_data.shm_lpid;
        } else if(mem_data.shm_nattch > 2){
            printf("Soy un proceso intruso. Procedo a terminar.\n");
            shmdt ((const void *) shm_ptrA);
            exit(6);
        } else {
            if(mem_data.shm_cpid == getpid()){
                printf("El proceso A fue el creador\n");
                PID_b=mem_data.shm_lpid;
            } else {
                printf("El proceso B fue el creador\n");
                PID_b=mem_data.shm_cpid;
            }
            
        }

        printf("Proceso B de pid %d\n\n",PID_b);

        /*le aviso al proceso B que ya se escribio*/
        if(kill(PID_b,SIGUSR1) != 0){
            flagT = 0;
        }

        if(kill(PID_b,SIGUSR2) != 0){
            flagT = 0;
        }

        while(flagT){
            if (canRead){

                for (int j=10;j<20; j++){
                    printf("Proceso A de PID = %d Leyendo memoria[%d]= '%c'\n",getpid(),j, int2char(shm_ptrA[j]));
                }
                printf("\n");

                canRead = 0;

                /*aviso que termine de leer*/
                if(kill(PID_b,SIGUSR2) != 0){
                    flagT = 0;
                    canWrite = 0;
                }
            }
            
            if (canWrite){

                for (int j=0; j<10; j++){
                    shm_ptrA[j] = char2int(cad[j],a,b);
                    printf("Proceso A con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrA[j]);
                    sleep (1);
                }
                printf("\n");

                canWrite = 0;

                /*aviso que termine de escribir*/
                if(kill(PID_b,SIGUSR1) != 0){
                    flagT = 0;
                }
            }

        }
        
        terminationA(shm_ptrA, PID_b, mem_id);
        exit(0);








    //Proceso B
    }else if (strcmp(argv[1],"b")== 0  || strcmp(argv[1],"B") == 0){
        printf("ejecutando el proceso B de PID: %d \n",getpid());

        int *shm_ptrB = NULL;
        pid_t PID_a;
        struct shmid_ds mem_data;
        char cad[] = "CHau MunDO";

		/*creacion de clave para identificar a la porcion de memoria compartida*/
		key = ftok (ARCHIVO,NUM);
		if (key == -1){
			printf("No consegui  clave para memoria compartida\n");
            exit(1);
		}

		/*creacion del espacio de memoria compartida*/
		mem_id = shmget(key, 22*sizeof(int), 0666| IPC_CREAT);
		if (mem_id == -1){
			printf("No consegui Id para memoria compartida\n");
			exit (2);
		}

		/*Asocicion de un puntero al espacio de memoria*/
		shm_ptrB = (int *)shmat(mem_id, (const void *)0, 0);
		if (shm_ptrB == NULL){
			printf("No consegui asociar la memoria compartida a una variable\n");
			exit (3);
		}

        

        shmctl(mem_id, IPC_STAT, &mem_data);
		/*verifico que el proceso A se haya ejecutado*/
        if(mem_data.shm_nattch == 1){
            printf("esperando a que se ejecute el proceso A ...\n");
            while(mem_data.shm_nattch<2){
                shmctl(mem_id, IPC_STAT, &mem_data);
                if(flagT == 0){
                    shmdt ((const void *) shm_ptrB);
                    printf("El proceso A no existe y se procede a terminar B...\n");
                    shmctl (mem_id, IPC_RMID, (struct shmid_ds *)NULL);
                    exit(5);
                } 
            }
            printf("el proceso B fue el creador\n");
            PID_a=mem_data.shm_lpid;
        } else if(mem_data.shm_nattch > 2){
            printf("Soy un proceso intruso. Procedo a terminar.\n");
            shmdt ((const void *) shm_ptrB);
            exit(6);
        } else{
            if(mem_data.shm_cpid == getpid()){
                printf("el proceso B fue el creador\n");
                PID_a=mem_data.shm_lpid;
            }
            else{
                printf("el proceso A fue el creador\n");
                PID_a=mem_data.shm_cpid;
            }
        }
        printf("Proceso A de pid %d\n\n",PID_a);

		while(flagT){
            
            if (canWrite){
                a = shm_ptrB[20];
                b = shm_ptrB[21];
                for (int j=10; j<20; j++){
                    shm_ptrB[j] = char2int(cad[j-10],a,b);
                    printf("Proceso B con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrB[j]);
                    sleep (1);
                }
                printf("\n");

                canWrite = 0;

                /*aviso que termine de escribir*/
                if(kill(PID_a,SIGUSR1) != 0){
                    flagT = 0;
                }
            } 

            if (canRead){

                for (int j=0;j<10; j++){
                    printf("Proceso B de PID = %d Leyendo memoria[%d]= '%c'\n",getpid(),j, int2char(shm_ptrB[j]));
                }
                printf("\n");

                canRead = 0;
                
                /*aviso que termine de leer*/
                if(kill(PID_a,SIGUSR2) != 0){
                    flagT = 0;
                    canWrite = 0;
                }
            }
            
        }

        /*desasocio el puntero*/
        terminationB(shm_ptrB, PID_a, mem_id);
		exit(1);
	}
    else{
        printf("identificador de proceso no reconocido\n");
        exit(4);
    }   
}


void handler_SIGUSR1(int sig1){
    canRead=1;
}

void handler_SIGUSR2(int sig2){
    canWrite=1;
}

void handler_SIGTERM(int sig3){
    flagT=0;
}

int char2int(char abc, int a, int b){
    int x=0;
    int fx=b;   // (a*0+b)mod(27)=5
    if(abc == ' '){
        x=0;
    }else if((abc>= 'a')&&(abc<='z')){
        x=abc-'a'+1;
    }else if((abc>= 'A')&&(abc<='Z')){
        x=abc-'A'+1;
    }

    return fx=(a*x+b)%27;
}

char int2char(int fx){
    char abc=' ';
    int x = 0;

    x = (7*fx+19)%27;

    if(x == 0){
        abc = ' ';
    } else {
        abc = ((unsigned char) x) + 'a' - 1; 
    }

    return abc;
}

void terminationA(int* shm_ptrA, pid_t PID_b, int mem_id){
    flagT = 1;
        if (kill(PID_b,SIGTERM) == 0){
            printf("Se da comienzo a la terminacion de los procesos.\n");
            while(flagT){
                sleep(1);
                if (kill(PID_b,SIGTERM) != 0){
                    break;
                }
            }
            printf("El proceso B ha terminado\n");
        } else {
            printf("El proceso B ha terminado inesperadamente.\nProcediendo a terminar el proceso A.\n");
        }
        

        /*desasocio el puntero*/
        shmdt ((const void *) shm_ptrA);
        /*remuevo la memoria compartida*/
        shmctl (mem_id, IPC_RMID, (struct shmid_ds *)NULL);
        return;
}

void terminationB(int* shm_ptrB,pid_t PID_a, int mem_id){
    shmdt ((const void *) shm_ptrB);
    if(kill(PID_a,SIGTERM) != 0){
        printf("El proceso A ha terminado inesperadamente\n");
        shmctl (mem_id, IPC_RMID, (struct shmid_ds *)NULL);
    }
    printf("Procediendo a terminar el proceso B\n");
    return;
}