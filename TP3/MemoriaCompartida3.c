#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define NUMERO 123
#define ARCHIVO "/bin/more"

/* Este programa usa la memoria compartida creada por memcomp1.c y muestra el uso de shmctl()*/
int main( int argc, char **argv)
{
	key_t Clave3;
	int Id_Memoria3;
	struct shmid_ds buf;

    /*-------------------------------------------------------------------------
    Ac� tambien tengo que conseguir una clave �nica pero igual a la de memcomp1
    Voy a usar el mismo  archivo 
    /bin/more  y el mismo numero que voy a definir con las Macros ARCHIVO y NUMERO
    -------------------------------------------------------------------------*/
	Clave3 = ftok (ARCHIVO, NUMERO);
	if (Clave3 == -1)
	{
		printf("No consegui  clave para memoria compartida\n");
		exit(1);
	}

	/* ----------------------------------------------------------------------
    Ahora tengo que solicitarle al Sistema Operativo que otorgue la memoria
    para compartir. 
    uso la llamada a sistema shmget() a la que tengo que pasarle:
    Clave
    Tama�o de memoria necesaria (en este caso voy a solicitar espacio 
    para 50 long) 
    Flags: los ultimos nueve bits corresponden a permisos en octal (rwxrwxrwx)
    para el due�o, el grupo y el resto del mundo respectivamente). 
    
    Ac� no uso IPC_CREAT porque supongo que ya lo creo el otro programa
    vamos a obtener un identificador del bloque de memoria creado 
    o -1 si hubo un error.
    -----------------------------------------------------------------------*/
	Id_Memoria3 = shmget (Clave3, 50*sizeof(long), 0666 );
	if (Id_Memoria3 == -1)
	{
		printf("No consegui Id para memoria compartida\n");
		exit (2);
	}
     
    /* ----------------------------------------------------------------------
    Vamos a usar el shmctl para otras cosas que para liberar la memoria.

    int shmctl(int shmid, int cmd, struct shmid_ds *buf);
    donde shmid es la identificaci�n que obtuvimos con shmget
    cmd puede tomar distintos valores, pero para liberar la memoria es IPC_RMID
    struct shmid_ds buf es una estructura que contienen informaci�n acerca del bloque 
    de memoria.

    struct shmid_ds {
               struct ipc_perm shm_perm;     Ownership and permissions
               size_t          shm_segsz;    Size of segment (bytes) 
               time_t          shm_atime;    Last attach time
               time_t          shm_dtime;    Last detach time 
               time_t          shm_ctime;    Last change time 
               pid_t           shm_cpid;     PID of creator 
               pid_t           shm_lpid;     PID of last shmat(2)/shmdt(2) 
               shmatt_t        shm_nattch;   No. of current attaches 
               ...
           };

    ----------------------------------------------------------------------*/
    shmctl (Id_Memoria3, IPC_STAT, &buf);	



	shmctl (Id_Memoria3, IPC_STAT, &buf);
/*Imprimimos la informaci�n obtenida:*/
printf("tama�o en bytes de shm:%lu\n",buf.shm_segsz);
printf("Cantidad de attaches:%lu\n",buf.shm_nattch);
printf("PID del creador:%d\n",buf.shm_cpid);
printf("PID del ultimo attach:%d\n",buf.shm_lpid);

/* a partir de aca terminamos el programa*/
exit(0);
}
