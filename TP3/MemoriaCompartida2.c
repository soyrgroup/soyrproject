#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define NUMERO 123
#define ARCHIVO "/bin/more"

/* Este programa usa la memoria compartida creada por memcomp1.c */
int main( int argc, char **argv)
{
	key_t Clave2;
	int Id_Memoria2;
	long *Memoria2 = NULL;
	int i,j;

    /*-------------------------------------------------------------------------
    Ac� tambien tengo que conseguir una clave �nica pero igual a la de memcomp1
    Voy a usar el mismo  archivo 
    /bin/more  y el mismo numero que voy a definir con las Macros ARCHIVO y NUMERO
    -------------------------------------------------------------------------*/
	Clave2 = ftok (ARCHIVO, NUMERO);
	if (Clave2 == -1)
	{
		printf("No consegui  clave para memoria compartida\n");
		exit(1);
	}

	/* ----------------------------------------------------------------------
    Ahora tengo que solicitarle al Sistema Operativo que otorgue la memoria
    para compartir. 
    uso la llamada a sistema shmget() a la que tengo que pasarle:
    Clave
    Tama�o de memoria necesaria (en este caso voy a solicitar espacio 
    para 50 long) 
    Flags: los ultimos nueve bits corresponden a permisos en octal (rwxrwxrwx)
    para el due�o, el grupo y el resto del mundo respectivamente). 
    
    Ac� no uso IPC_CREAT porque supongo que ya lo creo el otro programa
    vamos a obtener un identificador del bloque de memoria creado 
    o -1 si hubo un error.
    -----------------------------------------------------------------------*/
	Id_Memoria2 = shmget (Clave2, 50*sizeof(long), 0666 );
	if (Id_Memoria2 == -1)
	{
		printf("No consegui Id para memoria compartida\n");
		exit (2);
	}
    /* ----------------------------------------------------------------------
	Ahora debo asociar la identificaci�n de bloque de memoria con una 
    variable del programa, para eso uso la llamada al sistema shmat()

    void *shmat(int shmid, const void *shmaddr, int shmflg);

    Para ver otras posibilidades de flags, ver man shmat
    ----------------------------------------------------------------------*/
	Memoria2 = (long *)shmat (Id_Memoria2, (const void *)0, 0);
    
	if (Memoria2 == NULL)
	{
		printf("No consegui asociar la memoria compartida a una variable\n");
		exit (3);
	}

	 /* ----------------------------------------------------------------------
	Ya podemos usar la memoria compartida como una variable mas.
    en este caso vamos a leer desde la memoria los numeros que escribe el 
    otro programa y los vamos a mostrar en pantalla   a razon de 1 por segundo
    ----------------------------------------------------------------------*/
	
		for (j=0; j<50; j++)
		{
		//Memoria2[j] = j*2;
		printf("Proceso %d: Leido desde Memoria2[%d]= %ld\n",getpid(),j, Memoria2 [j]);
		sleep (1);
		}

	

    /* ----------------------------------------------------------------------
	Terminada de usar la memoria compartida, la liberamos.
    Como la responsabilidad de "devolver" el bloque de memoria al SO es del 
    Proceso 1, entonces solamente debemos eliminar la asociaci�n entre el 
    bloque de memoria y el puntero 
    (variable del programa) con shmdt()
    ----------------------------------------------------------------------*/
	
	
	shmdt ((const void *) Memoria2);

/* a partir de aca terminamos el programa*/
exit(0);
}
