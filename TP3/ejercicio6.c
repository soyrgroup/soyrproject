#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM 18
#define ARCHIVO "/bin/ls"

void handler_SIGTERM(int);

/*variables globales*/
char flagT=1;
char Lee_a,Lee_b,escribe_a,escribe_b;

int main( int argc, char *argv[])
{
    /* manejador de señales para SIGTERM*/
    signal(SIGTERM, handler_SIGTERM);

	key_t key;
	int mem_id;
	int status;


    //Proceso A
    if (strcmp(argv[1],"a") == 0  || strcmp(argv[1],"A") == 0){
        printf("ejecutando el proceso A de pid: %d \n",getpid());

        int *shm_ptrA = NULL;
        char flag1_A;
        struct shmid_ds mem_data;
        pid_t PID_b;

        /*creacion de clave para identificar a la porcion de memoria compartida*/
        key = ftok(ARCHIVO,NUM);
        if (key == -1){
            printf("No consegui clave para memoria compartida\n");
            exit(1);
        }

        /*creacion del espacio de memoria compartida*/
        mem_id = shmget(key, 20*sizeof(int), 0666 | IPC_CREAT);
        if (mem_id == -1){
            printf("No consegui ID para memoria compartida\n");
            exit (2);
        }

        /*Asocicion de un puntero al espacio de memoria*/
        shm_ptrA = (int *)shmat(mem_id, (const void *)0, 0);
        if (shm_ptrA == NULL){
            printf("No consegui asociar la memoria compartida a una variable\n");
            exit (3);
        }

        /*comienzo escribiendo la porcion de memoria que me corresponde*/
        for (int j=0; j<10; j++){
            shm_ptrA[j] = j*2;
            printf("Proceso A con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrA[j]);
            sleep(1);
        }
        /*aviso que termine de escribir*/
        Lee_b=1;
        /*verifico que el proceso B se haya ejecutado*/
        shmctl(mem_id, IPC_STAT, &mem_data);
        if(mem_data.shm_nattch<2){
            printf("esperando a que se ejecute el proceso B ...\n");
            while(mem_data.shm_nattch<2){
                shmctl(mem_id, IPC_STAT, &mem_data);
            }
            printf("el proceso A fue el creador\n");
            PID_b=mem_data.shm_lpid;
        } else {
            if(mem_data.shm_cpid == getpid()){
                printf("El proceso A fue el creador\n");
                PID_b=mem_data.shm_lpid;
            } else {
                printf("El proceso B fue el creador\n");
                PID_b=mem_data.shm_cpid;
            }
            
        }

        printf("Proceso B de pid %d\n",PID_b);

        while(flagT){
            if (Lee_a){
                /*no lo dejo escribir al proceso B hasta que termine de leer*/
                escribe_b=0;
                for (int j=10;j<20; j++){
                printf("Proceso A de PID = %d Leyendo memoria[%d]= %d\n",getpid(),j, shm_ptrA[j]);
                }
            }
            /*aviso que termine de leer*/
            escribe_b=1;
            
            if(escribe_a){
                /*no lo dejo leer al proceso B hasta que termine de escribir*/
                Lee_b=0;
                for (int j=0; j<10; j++){
                shm_ptrA[j] = j*2;
                printf("Proceso A con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrA[j]);
                sleep (1);
                }
                /*aviso que termine de escribir*/
                Lee_b=1;
            }
        }
        flagT = 1;
        printf("Soy el proceso A, esperando a que el proceso B termine...\n");
        while(flagT);
        //printf("El proceso B ha terminado con estado: %d\n", status);
        printf("El proceso B ha terminado\n");

        /*desasocio el puntero*/
        shmdt ((const void *) shm_ptrA);
        /*remuevo la memoria compartida*/
        shmctl (mem_id, IPC_RMID, (struct shmid_ds *)NULL);
        
        exit(0);




    //Proceso B
    }else if (strcmp(argv[1],"b")== 0  || strcmp(argv[1],"B") == 0){
        printf("ejecutando el proceso B de PID: %d \n",getpid());

        int *shm_ptrB = NULL;
        pid_t PID_a;
        struct shmid_ds mem_data;

		/*creacion de clave para identificar a la porcion de memoria compartida*/
		key = ftok (ARCHIVO,NUM);
		if (key == -1){
			printf("No consegui  clave para memoria compartida\n");
			exit(1);
		}

		/*creacion del espacio de memoria compartida*/
		mem_id = shmget(key, 20*sizeof(int), 0666| IPC_CREAT);
		if (mem_id == -1){
			printf("No consegui Id para memoria compartida\n");
			exit (2);
		}

		/*Asocicion de un puntero al espacio de memoria*/
		shm_ptrB = (int *)shmat(mem_id, (const void *)0, 0);
		if (shm_ptrB == NULL){
			printf("No consegui asociar la memoria compartida a una variable\n");
			exit (3);
		}

        /*comienzo escribiendo la porcion de memoria que me corresponde*/
        for (int j=10; j<20; j++){
			shm_ptrB[j] = j+2;
			printf("Proceso B con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrB[j]);
			sleep(1);
		}
        /*aviso que termine de escribir*/
        Lee_a=1;

        shmctl(mem_id, IPC_STAT, &mem_data);
		/*verifico que el proceso A se haya ejecutado*/
        if(mem_data.shm_nattch<2){
            printf("esperando a que se ejecute el proceso A ...\n");
            while(mem_data.shm_nattch<2){
                shmctl(mem_id, IPC_STAT, &mem_data);  
            }
            printf("el proceso B fue el creador\n");
            PID_a=mem_data.shm_lpid;
        }else{
            if(mem_data.shm_cpid == getpid()){
                printf("el proceso B fue el creador\n");
                PID_a=mem_data.shm_lpid;
            }
            else{
                printf("el proceso A fue el creador\n");
                PID_a=mem_data.shm_cpid;
            }
        }
        printf("Proceso A de pid %d\n",PID_a);


		while(flagT){
            sleep(5);
            if (Lee_b){
                /*no lo dejo escribir al proceso A hasta que termine de leer*/
                escribe_a=0;
                for (int j=0;j<10; j++){
                printf("Proceso B de PID = %d Leyendo memoria[%d]= %d\n",getpid(),j, shm_ptrB[j]);
                }
            }
            /*aviso que termine de leer*/
            escribe_a=1;
            
            if(escribe_b){
                /*no lo dejo leer al proceso A hasta que termine de escribir*/
                Lee_a=0;
                 for (int j=10; j<20; j++){
			        shm_ptrB[j] = j+2;
			        printf("Proceso B con PID = %d escribiendo Memoria[%d]= %d\n",getpid(),j,shm_ptrB[j]);
			        sleep (1);
		        }
                /*aviso que termine de escribir*/
                Lee_a=1;
            }
        }

        /*desasocio el puntero*/
    	shmdt ((const void *) shm_ptrB);
		kill(PID_a,SIGTERM);
		exit(1);
	}
    else{
        printf("identificador de proceso no reconocido\n");
        exit(4);
    }   
}


void handler_SIGTERM(int sig3){
    flagT=0;
}