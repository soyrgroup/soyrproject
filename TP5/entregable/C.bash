ifconfig eth0 220.189.236.125/26
ifconfig eth1 220.189.236.190/26
ifconfig eth2 220.189.236.254/26
ifconfig eth3 220.189.101.254/30
route add default gw 220.189.236.126 metric 10
route add -net 220.189.232.0/24 gw 220.189.236.126
route add -net 220.189.232.0/24 gw 220.189.236.189 metric 1
route add -net 220.189.234.0/24 gw 220.189.236.126
route add -net 220.189.234.0/24 gw 220.189.236.189 metric 1
route add -net 220.189.100.0/24 gw 220.189.236.126
route add -net 220.189.100.0/24 gw 220.189.236.189 metric 1
route add -net 220.189.101.0/24 gw 220.189.101.253
route add -net 220.189.235.0/24 gw 220.189.236.189
route add -net 220.189.235.0/24 gw 220.189.236.126 metric 1
