# Redes de Computadoras

El archivo topologia_entregable.imn contiene un programa para emular el funcionamiento de las redes de computadoras presentada en el trabajo de los autores. Se debe abrir el archivo desde la aplicación CORE Network Emulator.

## Configuración de los dispositivos de red

En primera instancia se debe poner en marcha la sesión presionando el botón play verde a la izquierda de la interfaz. Luego, presionando dos veces sobre cualquier dispositivo para abrir una ventana de comandos, se debe escribir la siguiente instrucción de bash.

```bash
restore < direccion de los archivos de configuracion >
```

## Prueba de conexión con el comando ping
Presionando dos veces sobre cualquier dispositivo para abrir una ventana de comandos, se debe escribir la siguiente instrucción de bash.

```bash
ping < ip destino >
```
