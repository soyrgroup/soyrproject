ifconfig eth0 220.189.232.50/24
ifconfig eth1 220.189.234.254/26
ifconfig eth3 220.189.236.126/26
ifconfig eth4 220.189.235.126/26
ifconfig eth2 220.189.100.254/30
route add default gw 220.189.232.1 metric 10
route add -net 220.189.100.0/24 gw 220.189.100.253
route add -net 220.189.101.0/24 gw 220.189.236.125
route add -net 220.189.101.0/24 gw 220.189.235.125 metric 1
route add -net 220.189.236.0/24 gw 220.189.236.125
route add -net 220.189.236.0/24 gw 220.189.235.125 metric 1
route add -net 220.189.235.0/24 gw 220.189.235.125
route add -net 220.189.235.0/24 gw 220.189.236.125 metric 1
