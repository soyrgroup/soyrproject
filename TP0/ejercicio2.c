#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define Tam_User_name   60
#define Tam_User_email  40
#define Tam_file_name   40

typedef struct
{
     char Nombre_de_Usario[Tam_User_name];
     unsigned int ID;
  	 char email[Tam_User_email];
}Usuario;

int Escribe_Datos_usuario(Usuario *User_ptr,unsigned);
void Lee_Datos_usuario(Usuario *User_ptr);

int main()
{

    Usuario user;
    unsigned i=1;

    while(Escribe_Datos_usuario(&user,i)!=-1)
    {
        char nombre[Tam_file_name];
        printf("Nombre del archivo: ");
        gets(nombre);
        strcat(nombre,".txt");

        FILE *pf;

        /* Se abre el archivo para escritura*/
        pf=fopen(nombre,"ab");

        if(pf==NULL)
        {
        printf("error al abrir el archivo");
        return -1;
        }

        fwrite(&user,sizeof(Usuario),1,pf);

        fclose(pf);

        pf=fopen(nombre,"rb");

        if(pf==NULL)
        {
        printf("error al abrir el archivo");
        return -1;
        }

        fseek(pf,0,SEEK_END);
        int tam=ftell(pf)/sizeof(Usuario);
        printf("el archivo contiene %d usuarios \n",tam);

        Usuario user2;

        /* Se posiciona para leer el ultimo usuario*/
        fseek(pf,-sizeof(Usuario),SEEK_END);

        fread(&user2,sizeof(Usuario),1,pf);
        fclose(pf);

        Lee_Datos_usuario(&user2);

        i++;//incremento ID
    }


    return 0;
}

int Escribe_Datos_usuario(Usuario *User_ptr,unsigned i)
{
    printf("Nombre de Usuario:");
    gets(User_ptr->Nombre_de_Usario);
    if (strlen(User_ptr->Nombre_de_Usario)==0)
    {
    return -1;
    }

    printf("direccion de correo electronico:");
    gets(User_ptr->email);

    User_ptr->ID=i;
    printf("ID de Usuario= %d\n\n",User_ptr->ID);

    return 0;
}

void Lee_Datos_usuario(Usuario *User_ptr)
{
    printf("Nombre de Usuario:");
    puts(User_ptr->Nombre_de_Usario);

    printf("direccion de correo electronico:");
    puts(User_ptr->email);

    User_ptr->ID=1;
    printf("ID de Usuario= %d\n\n",User_ptr->ID);
}


